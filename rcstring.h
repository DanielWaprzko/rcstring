#ifndef __RCSTRING_H__
#define __RCSTRING_H__
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <iostream>
using namespace std;

class rcstring{
  struct rctext;
  rctext* data;
public:
class Range{};
/* Klasa ktora ma za zadanie odroznienie czytanai od pisania */
class Cref;
rcstring();
int atoi();
rcstring& toLower();
rcstring Left(int y);
rcstring(const char*);
rcstring(const rcstring&);
~rcstring();
/*Przeciazanie operatorow polega na nadawaniu im nowcyh funkcji (przykladowo dla operatora + mozemy zadeklarowac odejmowanie - co nie znacyz ze powinnismy tak robic)*/
/* & - oznacza przekazywanie argumentow do funkcji za pomoca referencji ( operacje na parametrach formalnych odnosza sie do parametrow aktualnych) */
rcstring& operator=(const char*);

/* const & - oznacza do funkcji przekazywana jest referencja do parametru w celu unikniecia kosztow kopiowania ale wartosc nie moze byc w funkcji modyfikowana(kompilator sprawdza */
/*"= " - operator przypisania */
rcstring& operator=(const rcstring&);

/* operator z przypisaniem - Wykonuje okresloan operacje i zapisuje wynik w lewym argumencie , jest to polaczenie operatora arytmetyczniego/bitowego z operatorem dopisania */
rcstring& operator+=(const rcstring &);

/* operator dodawania */
rcstring operator+(const rcstring &) const;

/* operator bitowy */
/* klasa zaprzyjazniona , deklaracje friend stosuje sie w momencie gdy dana funkcja/klasa potzrebuje uzyskac dostep do prywatnych lub chronionych skladowych klasy
	deklaracje umieszcza sie w klasie, ktora ma udostepniac swoje skladowe */
friend ostream& operator<<(ostream&, const rcstring&);
/* funkcja ktora nei moze nadpisywac  danych - const */
void check (unsigned int i) const;
char read(unsigned int i) const;
void write(unsigned int i, char c);

/* operator indeksu [] ktory moze byc zdefiniowany wylacznie jako metoda */
char operator[](unsigned int i) const;
Cref operator[](unsigned int i);
};

/* Klasa jako uchwyt - zawiera wskażnik do rzeczywistej implementacji łańcucha*/

struct rcstring::rctext /*Tworzymy strukture której zadanie polega na zliczaniu odwołań w celu minimalizacji alokacji pamięci i kopiowań */
{
 /* z powodu braku wbudowanego trybu łańcuchowego jesteśmy zmuszeni korżystać z formy char* która jest podatna na błedy */
  char* s;  /* nasz ciąg znakow */
  unsigned int size; /* rozmiar ciągu znaków */
  unsigned int n; /*wspołczynnik liczności odwołań */

  rctext(unsigned int nsize, const char* p)
  {
    n=1;
    size=nsize;
    s=new char[size+1];
    strncpy(s,p,size); /* kopiowanie do "s" z miejsca "p" danych o rozmiarze "size" */
    s[size]='\0'; /*do wykrycia nulla */
  };
  ~rctext() /*destruktor */
  {
    delete [] s; /*Musimy zadeklarowac czyszczenie pamięci po wskaźnikach , chroni przed wyciekiem pamięci */
  };
  rctext* detach() /* funkcja powodująca rozdzielenie kopii łańcucha oraz zmniejszenie współczynnika liczności odwołań */
  {
    if(n==1)
      return this; /*this - jest to wskaznik ktory jest przekazywany jako dodatkowy, niejawny argument do wszystkich metod i niejawnie uzywany do odwolywania sie do pol klasy*/
			/*Jesli n==1 zwracamy this czyli rctext */
    rctext* t=new rctext(size, s); /*Dodatkowy współczynnik liczności odwołan t */
    n--; 
    return t;
  };
  void assign(unsigned int nsize, const char *p)
  {
    if(size!=nsize)
    {
      char* ns=new char[nsize+1];
      size = nsize;
      strncpy(ns,p,size);
      delete [] s;
      s = ns;
    }
    else
      strncpy(s,p,size);
    s[size]='\0';
  }
private:
  rctext(const rctext&);
  rctext& operator=(const rctext&);
};

class rcstring::Cref
{
  friend class rcstring;
  rcstring& s;
  int i;
  Cref (rcstring& ss, unsigned int ii): s(ss), i(ii) {};
public:
  operator char() const
  {
    cout << "operator char() const"<<endl;
    return s.read(i);
  }
  rcstring::Cref& operator = (char c)
  {
    cout << "void operator = (char c)"<<endl;
    s.write(i,c);
    return *this;
  }
  rcstring::Cref& operator = (const Cref& ref)
  {
    return operator= ((char)ref);
  }
};
/* inline - funkcje otwarte - rozwijane w miejscu wywolania */
inline rcstring::rcstring()
  {
    data = new rctext(0,"");
  }

inline rcstring::rcstring(const rcstring& x)
  {
    x.data->n++;
    data=x.data;
  }
inline rcstring::~rcstring()
{
  if(--data->n==0)
    delete data;
}

rcstring& rcstring::operator=(const rcstring & x)
{
  x.data->n++;
  if(--data->n == 0)
    delete data;
  data=x.data;
  return *this;
}

rcstring::rcstring(const char* s)
{
 data=new rctext(strlen(s),s);
}

rcstring& rcstring::operator=(const char* s)
{
  if(data->n==1)
    data->assign(strlen(s),s);
  else
  {
    rctext* t = new rctext(strlen(s),s);
    data->n--;
    data= t;
  };
  return *this;
}

ostream& operator << (ostream& o, const rcstring& s)
{
  return o<<s.data->s;
}

rcstring& rcstring::operator+=(const rcstring & s)
{
unsigned int newsize=data->size+s.data->size;
rctext *newdata=new rctext(newsize,data->s);
strcat(newdata->s,s.data->s);
if(--data->n==0)
  delete data;
data = newdata;
return *this;
}

rcstring rcstring::operator+(const rcstring & s) const
{
  return rcstring(*this)+=s;
}

/*IMPLEMENTACJA NOWYCH METOD */

/* ATOI - konwersja lancucha znaków na liczbe */
int rcstring::atoi(){
	int a = 0;
	int i = 0;
	while (data->s[i] >= '0' && data->s[i] <= '9'){
	a = a * 10 + data->s[i] - '0';
	i++;
	}
	return a;	
}
/* ToLower - Konwersja wielkich liter na male */
 rcstring& rcstring::toLower(){
	char *nowa = new char(data->size);
	int i = 0;
	while (i < data->size){
		if((int)data->s[i] >= 'A' && (int)data->s[i] <= 'Z')
			nowa[i] = data->s[i] + ('a' - 'A');
		else
			nowa[i] = data->s[i];
			i++;
	}	
	rctext *nowytext;
	try{
		nowytext = new rctext(data->size,nowa);
    	}
	catch(...){
		delete[] nowa;
		throw;
	}
	delete[] nowa;
	if(--data->n==0)
		delete data;
	data = nowytext;
	return *this;
}		

 /* Left - ekstrakcja n znakow od Lewej */
 rcstring rcstring::Left(int y){
	char *nowy = new char(y+1);
	int i = 0;
	while (i < y){
			nowy[i] = data->s[i];
			i++;		
	}
	nowy[y]='\0';	
	rcstring nowszy;

	try{
	nowszy = rcstring(nowy);
	}
		catch(...){
			delete [] nowy;
			throw;
		}
	delete [] nowy;
	return nowszy;
	}	


inline void rcstring::check (unsigned int i) const
{
if(data->size<=i)
  throw Range();
}
inline char rcstring::read(unsigned int i) const
{
 return data->s[i];
}
inline void rcstring::write(unsigned int i, char c)
{
  data = data->detach();
  data->s[i] = c;
}

char rcstring::operator[](unsigned int i) const
{
  cout << "char rcstring::operator[](unsigned int i) const"<<endl;
  check(i);
  return data->s[i];
}

rcstring::Cref rcstring::operator[](unsigned int i)
{
  cout << "Cref rcstring::operator[](unsigned int i)"<<endl;
  check(i);
  return Cref(*this,i);
}


#endif /* __RCSTRING_H__ */
